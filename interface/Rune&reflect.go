package main

import (
	"fmt"
	"reflect"
)

func main() {
	temp := "Tarik"
	fmt.Printf("data type of your variable is %T\n", temp)
	fmt.Println(reflect.TypeOf(temp))
	rune := 'A'
	fmt.Println(rune)
	fmt.Print(reflect.TypeOf(rune))
}

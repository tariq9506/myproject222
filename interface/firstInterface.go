package main

import (
	"fmt"
	"math"
)

type shape interface {
	area() float64
}

type circle struct {
	redious float64
}

type rect struct {
	hight float64
	width float64
}

func (c circle) area() float64 { //here area is a methode which contain reciver (r reac) it is differ from function
	return math.Pi * c.redious * c.redious
}

func (r rect) area() float64 {  //here area is a methode which contain reciver (r reac) it is differ from function
	return r.hight * r.width
}

func main() {

	c := circle{5}
	r := rect{4, 8}

	myInterr := []shape{c, r}

	for _, shape := range myInterr {

		fmt.Println(shape.area())
	}

}

package main

import (
	"fmt"
)

type Employee struct {
	name   string
	salary float64
}

func (e Employee) getname() {
	fmt.Println("Employee name", e.name)
}

func (e Employee) getsalary(tax float64) float64 {
	return (e.salary - ((e.salary / 100) * tax))
}

type print interface {
	getname()
	getsalary(float64) float64
}

func main() {

	e1 := Employee{"Tarik", 20000}
	e1.getname()
	var p1 print
	p1 = e1
	s := p1.getsalary(32)
	//s = p1.getsalary(32)
	fmt.Print("your salary : ", s)

}

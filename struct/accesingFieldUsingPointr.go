package main

import "fmt"

type Add struct {
	street, city, state string
	pin                 int
}

func main() {

	add := &Add{"kajiana", "kanpur", "up", 209214}
	fmt.Println("your state = ", (*add).state)
}

package main

import "fmt"

type Student struct {
	f_name, m_name, l_name string
	id                     int
}

func main() {

	b := Student{}
	fmt.Println(b)

	a := Student{"MOHD", "TARIK", "ANSARI", 2}
	fmt.Println(a)
	fmt.Println("first name = ", a.f_name)

}

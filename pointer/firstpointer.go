package main

import (
	"fmt"
)

func swap(ptr1 *int, ptr2 *int) {

	temp := ptr1
	ptr1 = ptr2
	ptr2 = temp
	fmt.Println("value of ptr1 is", *ptr1, "value of ptr2 is", *ptr2)

}

func main() {

	var i, j int
	i = 10
	j = 20
	fmt.Println("value of i is", i, "value of j is", j)
	fmt.Print(&i)
	//var ptr *int
	//ptr = &i
	//fmt.Println(ptr)
	//fmt.Print(*ptr)
	swap(&i, &j)
	//fmt.Println("value of ptr1 is", i, "value of ptr2 is", &j)
	fmt.Print(&i)
}

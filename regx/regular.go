package main

import (
	"fmt"

	"regexp"
)

func main() {
	str := "MoTarikaaaaAnsari"
	Match1, err := regexp.MatchString("Tarik", str)

	fmt.Println("Match", Match1, "Error\n", err)

	str1 := "Computer Science from KIT MCA"
	match2, err2 := regexp.MatchString(" Kit", str1)
	fmt.Println("Match = ", match2, " error  = ", err2)

	str2 := "From Kanpur Majhawan 209214 uttarPradesh"
	match2, err1 := regexp.MatchString("Majhawan (", str2)
	fmt.Println("match = ", match2, "error = ", err1)
}

package main

import (
	"fmt"
	"regexp"
	"strings"
	//"strings"
)

func main() {
	st, _ := regexp.Compile("Tariq ")
	str := "Mo Tariq Ansari"
	//to find index of string given in compile function
	match := st.FindStringIndex(str)
	fmt.Println(match)
	str1 := "Computer Science "
	match1 := st.FindStringIndex(str1)
	fmt.Println(match1)
	// use ReplaceAllString to replace string with given perticular character
	str2 := "Hello tarik ansari how are you"
	st1, err := regexp.Compile(" ")
	match2 := st1.ReplaceAllString(str2, "/")
	fmt.Println(match2, err)
	// Hello/tarik/ansari/how/are/you
	//use of ReplaceAllStringFunc to convert your string uppercase or lowrcase acc to your choice
	str3 := "assalamualaikum "
	st2, _ := regexp.Compile("[aeiou]")
	match3 := st2.ReplaceAllStringFunc(str3, strings.ToUpper)
	fmt.Println(match3)
	//AssAlAmUAlAIkUm

}

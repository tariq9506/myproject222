package main

import (
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	p := Person{Name: "Alice", Age: 30}
	jsonBytes, err := json.Marshal(p)
	if err != nil {
		// handle error
	}
	jsonString := string(jsonBytes)
	fmt.Println(jsonString)
}

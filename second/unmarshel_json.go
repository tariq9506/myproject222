package main

import (
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	jsonString := `{"name":"Alice","age":30}`
	var p Person
	err := json.Unmarshal([]byte(jsonString), &p)
	if err != nil {
		// handle error
	}
	fmt.Println(p)
}
